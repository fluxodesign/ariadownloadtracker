package net.fluxo.adt

import java.io.{PrintWriter, StringWriter}

import org.apache.log4j.{Level, Logger}
import org.joda.time.DateTime
import org.joda.time.format.{DateTimeFormat, DateTimeFormatter}

/**
 * @author Ronald Kurniawan (viper)
 * @version 0.1, 6/08/15
 */
object LogWriter {

	/**
	 * Writes a message into the log file, and depending on the Level, should trigger a Mail response.
	 *
	 * @param message string to write into the log
	 * @param level <code>org.apache.log4j.Level</code> object
	 */
	def writeLog(message: String, level: Level) {
		val rootLogger = Logger.getRootLogger
		val mailLogger = Logger getLogger "net.fluxo.MailLogger"
		val isMailAppenderActive = mailLogger.getLevel != Level.OFF
		level match {
			case Level.WARN =>
				rootLogger warn message
				if (isMailAppenderActive) mailLogger warn message
			case Level.TRACE =>
				rootLogger trace message
				if (isMailAppenderActive) mailLogger trace message
			case Level.INFO =>
				rootLogger info message
			//if (isMailAppenderActive) mailLogger.info(message)
			case Level.FATAL =>
				rootLogger fatal message
				if (isMailAppenderActive) mailLogger fatal message
			case Level.ERROR =>
				rootLogger error message
				if (isMailAppenderActive) mailLogger error message
			case Level.DEBUG =>
				rootLogger debug message
			//if (isMailAppenderActive) mailLogger.debug(message)
		}
	}

	/**
	 * Returns a string representation of current date and time.
	 *
	 * @return a string of current date and time in the format of 'yyyy-MM-dd HH:mm:ss TZ'
	 */
	def currentDateTime: String = {
		val formatter: DateTimeFormatter = DateTimeFormat forPattern "yyyy-MM-dd HH:mm:ss ZZZZ"
		formatter print DateTime.now().getMillis
	}

	/**
	 * Turns an exception stack trace into a <code>String</code>.
	 *
	 * @param e Exception
	 * @return a string containing the stack trace of the Exception
	 */
	def stackTraceToString(e: Throwable): String = {
		val sw = new StringWriter
		val pw = new PrintWriter(sw)
		e printStackTrace pw
		sw.toString
	}
}
