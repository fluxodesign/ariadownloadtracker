package net.fluxo.adt

import java.net.URLEncoder
import java.util
import javax.ws.rs.client.Entity

import org.apache.log4j.Level
import org.apache.xmlrpc.XmlRpcException
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder

/**
 * AriaDownloadTracker
 *
 * This application
 * @author Ronald Kurniawan (viper)
 * @version 0.1, 1/08/15
 */
object DownloadTracker {

	var _sGID: Option[String] = None
	var _sOwner: Option[String] = None
	var _iPort: Int = -1
	var _sURL: Option[String] = None
	var _bDebug: Boolean = false

	/**
	 * We should receive 3 parameters: GID of the download, RPC port number
	 * and the URL of dd's web service endpoints
	 * @param args array of parameters
	 */
	def main(args: Array[String]) {
		if (args.length < 8) {
			printHelp()
			return
		}
		var argIndex = 0
		while (argIndex < args.length) {
			args(argIndex) match {
				case "-g" =>
					_sGID = Some(args(argIndex + 1))
					argIndex += 2
				case "-p" =>
					_iPort = {
						try {
							args(argIndex + 1).toInt
						} catch {
							case ex: Exception => -1
						}
					}
					argIndex += 2
				case "-u" =>
					_sURL = Some(args(argIndex + 1))
					argIndex += 2
				case "-o" =>
					_sOwner = Some(args(argIndex + 1))
					argIndex += 2
				case "-d" => _bDebug = true; argIndex += 1
			}
		}

		if (_sGID.isEmpty || _iPort < 0 || _sURL.isEmpty || _sOwner.isEmpty) {
			printHelp()
			return
		}

		// Now that everything is ready, we begin...
		track()
	}

	def printHelp() {
		Console.println("syntax: scala -classpath <path/to/file.jar> net.fluxo.adt.DownloadTracker <parameters>")
		Console.println("where parameters are:")
		Console.println("\t-g [GID]\t16-characters of download GID")
		Console.println("\t-p [PORT]\tRPC port number of aria process")
		Console.println("\t-o [OWNER]\tProcess owner identification")
		Console.println("\t-u [URL]\tURL of download daemon's REST endpoint that deals with tracking")
		Console.println("\t-d\toutput debug comments (can be omitted)")
	}

	def track() {
		var isFinished = false
		while (!isFinished) {
			// sleep for 8 seconds
			Thread sleep 8000
			// we need to send status request to rpc server of this aria process, extract the information returned,
			// update the database by sending PUT request to dd's web service endpoint, then sleep for 8 seconds
			// before we do it all over again
			var followedBy: Option[String] = None
			var activeGID: Option[String] = None
			var completedLength: Long = 0L
			var totalLength: Long = 0L
			var packageName: Option[String] = None
			var infoHash: Option[String] = None
			var ariaPID: Option[String] = None
			try {
				if (_bDebug) {
					Console.println("New loop: " + System.currentTimeMillis())
					LogWriter writeLog("New loop: " + System.currentTimeMillis(), Level.INFO)
				}
				val xmlRpcClient = OUtils.getXmlRpcClient(_iPort)
				val rpcObject = OUtils.sendAriaTellStatus(_sGID.orNull, xmlRpcClient)
				// extracting information returned...
				val jmap = rpcObject.asInstanceOf[util.HashMap[String, Object]]
				if (_bDebug) {
					Console.println(jmap.toString)
					LogWriter writeLog("Returned value: " + jmap.toString, Level.INFO)
				}
				ariaPID = Some(OUtils findAriaTaskPid _sGID.orNull)
				if (_bDebug && ariaPID.isDefined) {
					Console.println("--> aria2c PID: " + ariaPID.orNull)
					LogWriter writeLog("--> aria2c PID: " + ariaPID.orNull, Level.INFO)
				}
				val objFollowedBy = OUtils extractValueFromHashMap(jmap, "followedBy")
				if (_bDebug) {
					Console.println("FOLLOWED-BY")
					LogWriter writeLog("FOLLOWED-BY", Level.INFO)
				}
				if (objFollowedBy.isDefined) {
					val arrFollowedBy = objFollowedBy.orNull.asInstanceOf[Array[Object]]
					for (o <- arrFollowedBy) {
						followedBy = Some(o.toString)
						if (_bDebug) {
							Console.println(o.toString)
							LogWriter writeLog(o.toString, Level.INFO)
						}
					}
				} else if (_bDebug) {
					Console.println("--> null")
					LogWriter writeLog("--> null", Level.INFO)
				}
				if (_bDebug) {
					Console.println("ACTIVE")
					LogWriter writeLog("ACTIVE PROCESS", Level.INFO)
				}
				val activeRpc = OUtils.sendAriaTellActive(xmlRpcClient)
				// dump for DEBUG
				if (activeRpc != null) {
					for (o <- activeRpc) {
						if (_bDebug) {
							Console.println(o.toString)
							LogWriter writeLog(o.toString, Level.INFO)
						}
						val activeMap = o.asInstanceOf[util.HashMap[String, Object]]

						activeGID = Some((OUtils extractValueFromHashMap(activeMap, "gid")).orNull.toString)
						// Getting the package name is a bit involved...
						val btorrentMap = (OUtils extractValueFromHashMap(activeMap, "bittorrent")).orNull.asInstanceOf[util.HashMap[String, Object]]
						val btInfoMap = (OUtils extractValueFromHashMap(btorrentMap, "info")).orNull.asInstanceOf[util.HashMap[String, Object]]
						packageName = Some((OUtils extractValueFromHashMap(btInfoMap, "name")).orNull.toString)

						completedLength = (OUtils extractValueFromHashMap(activeMap, "completedLength")).orNull.toString.toLong
						totalLength = (OUtils extractValueFromHashMap(activeMap, "totalLength")).orNull.toString.toLong
						infoHash = Some((OUtils extractValueFromHashMap(activeMap, "infoHash")).orNull.toString)
						sendUpdateInfo(_sOwner.orNull, _sGID.orNull, activeGID.orNull, totalLength, completedLength, packageName.orNull, infoHash.orNull)

						// finish the process and kill aria2c process....
						if (totalLength == completedLength && ariaPID.isDefined) {
							sendFinishedSignal(_sOwner.orNull, _sGID.orNull, activeGID.orNull, totalLength, completedLength, packageName.orNull, infoHash.orNull)
							OUtils sendAriaTellShutdown xmlRpcClient
							//Runtime.getRuntime exec "kill -9 " + ariaPID.orNull
							isFinished = true
						}
					}
				}
			} catch {
				case ie: InterruptedException => Console println "Sleep interrupted!"
				case xmlrpce: XmlRpcException => Console println("Failed to connect: " + xmlrpce.getMessage)
				case e: Exception => Console println "General exception: " + e.getMessage
			}
		}
	}

	/**
	 * Sample URL: http://[my-url]:[myport]/comm/rs/ws/trackerupdate
 	 */
	def sendUpdateInfo(owner: String, gid: String, activeGid: String, totalLength:Long, completedLength:Long, packageName: String, infoHash: String) {
		if (_sURL.isDefined) {
			val updateEndPoint: String = {
				if (_sURL.orNull endsWith "/") _sURL.orNull + "comm/rs/ws/trackerupdate"
				else _sURL.orNull + "/comm/rs/ws/trackerupdate"
			}
			val tData = new StringBuilder
			tData append "owner=" append owner append "&originalGid=" append gid append "&activeGid=" append activeGid
			tData append "&totalLength=" append totalLength append "&completedLength=" append completedLength
			tData append "&packageName=" append (URLEncoder encode(packageName, "UTF-8"))
			tData append "&infoHash=" append infoHash
			// send the request
			if (_bDebug) {
				Console println "TEXT DATA: " + tData.toString
				LogWriter writeLog("TEXT DATA: " + tData.toString, Level.INFO)
			}
			val reClient = new ResteasyClientBuilder() build()
			val target = reClient target updateEndPoint
			val reResponse = target.request.post(Entity.entity(tData.toString(), "text/plain"))
			if (_bDebug) {
				Console print "--> SENDING UPDATE: "
				Console println reResponse.getStatusInfo.getStatusCode + " " + reResponse.getStatusInfo.getReasonPhrase
				LogWriter writeLog("--> SENDING UPDATE: " + reResponse.getStatusInfo.getStatusCode +
					" (" + reResponse.getStatusInfo.getReasonPhrase + ")", Level.INFO)
			}
		}
	}

	/**
	 * Sample URL: http://[my-url]:[myport]/comm/rs/ws/trackerfinish
	 */
	def sendFinishedSignal(owner: String, gid: String, activeGid: String, totalLength:Long, completedLength:Long, packageName:String, infoHash: String) {
		if (_sURL.isDefined) {
			val finishEndpoint: String = {
				if (_sURL.orNull endsWith "/") _sURL.orNull + "comm/rs/ws/trackerfinish"
				else _sURL.orNull + "/comm/rs/ws/trackerfinish"
			}
			val tData = new StringBuilder
			tData append "owner=" append owner append "&originalGid=" append gid append "&activeGid=" append activeGid
			tData append "&totalLength=" append totalLength append "&completedLength=" append completedLength
			tData append "&packageName=" append (URLEncoder encode(packageName, "UTF-8")) append "&infoHash=" append infoHash
			if (_bDebug) {
				Console println "TEXT DATA: " + tData.toString
				LogWriter writeLog("TEXT DATA: " + tData.toString, Level.INFO)
			}
			// send the request
			val reClient = new ResteasyClientBuilder() build()
			val target = reClient target finishEndpoint
			val reResponse = target.request.post(Entity.entity(tData.toString(), "text/plain"))
			if (_bDebug) {
				Console print "--> SENDING FINISH SIGNAL: "
				Console println reResponse.getStatusInfo.getStatusCode + " " + reResponse.getStatusInfo.getReasonPhrase
				LogWriter writeLog("--> SENDING FINISH SIGNAL: " + reResponse.getStatusInfo.getStatusCode +
					" (" + reResponse.getStatusInfo.getReasonPhrase + ")", Level.INFO)
			}
		}
	}
}
