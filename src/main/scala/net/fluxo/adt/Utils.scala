package net.fluxo.adt

import java.io.ByteArrayOutputStream
import java.net.URL
import java.security.SecureRandom
import java.security.cert.X509Certificate
import java.util
import javax.net.ssl._

import org.apache.commons.exec.{CommandLine, DefaultExecutor, PumpStreamHandler}
import org.apache.xmlrpc.XmlRpcException
import org.apache.xmlrpc.client.{XmlRpcClient, XmlRpcClientConfigImpl}
import org.apache.xmlrpc.common.{TypeFactoryImpl, XmlRpcController, XmlRpcStreamConfig}
import org.apache.xmlrpc.serializer.{StringSerializer, TypeSerializer}
import org.xml.sax.{ContentHandler, SAXException}

/**
 * @author Ronald Kurniawan (viper)
 * @version 0.1, 1/08/15
 */
class Utils {

	val trustingManager: Array[TrustManager] = Array[TrustManager] {
		new X509TrustManager {
			override def getAcceptedIssuers: Array[X509Certificate] = {
				null
			}

			override def checkClientTrusted(x509Certificates: Array[X509Certificate], s: String) {}

			override def checkServerTrusted(x509Certificates: Array[X509Certificate], s: String) {}
		}
	}

	val sc: SSLContext = SSLContext getInstance "SSL"

	val hv: HostnameVerifier = {
		new HostnameVerifier {
			override def verify(s: String, sslSession: SSLSession): Boolean = true
		}
	}

	def getXmlRpcClient(port: Int): XmlRpcClient = {
		sc.init(null, trustingManager, new SecureRandom())
		val url = "http://127.0.0.1:" + port + "/rpc"
		val xmlClientConfig: XmlRpcClientConfigImpl = new XmlRpcClientConfigImpl()
		xmlClientConfig setServerURL new URL(url)
		val client = new XmlRpcClient()
		client setConfig xmlClientConfig
		client setTypeFactory new XmlRpcTypeFactory(client)
		client
	}

	/**
	 * Send a query status message to the specified <code>XmlRpcClient</code>.
	 *
	 * @param gid the unique download ID to query
	 * @param client a <code>org.apache.xmlrpc.client.XmlRpcClient</code> object
	 * @throws org.apache.xmlrpc.XmlRpcException XmlRpcException
	 * @return an XML object representing the download status
	 */
	@throws(classOf[XmlRpcException])
	def sendAriaTellStatus(gid: String, client: XmlRpcClient): Object = {
		//val params = Array[Object](gid)
		val params = new util.ArrayList[Object]()
		params add gid
		client execute("aria2.tellStatus", params)
	}

	/**
	 * Send a query to ask for active processes to the specified <code>XmlRpcClient</code>.
	 *
	 * @param client a <code>org.apache.xmlrpc.client.XmlRpcClient</code> object
	 * @throws org.apache.xmlrpc.XmlRpcException XmlRpcException
	 * @return an array of Objects representing the active downloads
	 */
	@throws(classOf[XmlRpcException])
	def sendAriaTellActive(client: XmlRpcClient): Array[Object] = {
		val params = Array[Object]()
		val returned = client.execute("aria2.tellActive", params)
		// Returned XML-RPC is an Array Java HashMap...
		returned.asInstanceOf[Array[Object]]
	}

	/**
	 * Send the command to shutdown an aria2 process connected to the specified <code>XmlRpcClient</code>.
	 *
	 * @param client a <code>org.apache.xmlrpc.client.XmlRpcClient</code> object
	 */
	def sendAriaTellShutdown(client: XmlRpcClient) {
		client.execute("aria2.forceShutdown", Array[Object]())
	}

	/**
	 * Returns a value <code>Object</code> out of a <code>HashMap</code>.
	 *
	 * @param map a <code>java.util.HashMap</code> object
	 * @param key a <code>java.utils.String</code> key to the Map
	 * @return an Object
	 */
	def extractValueFromHashMap(map: java.util.HashMap[String, Object], key:String): Option[Object] = {
		var ret: Option[Object] = None
		val it = map.entrySet().iterator()
		while (it.hasNext) {
			val entry = it next()
			if (entry.getKey.equals(key)) ret = Some(entry.getValue)
		}
		ret
	}

	def findAriaTaskPid(gid: String): String = {
		//val command = new StringBuilder
		//command append "ps aux | grep -e 'aria2c' | grep -e '" append gid append "' | awk {'print $2'}"

		val c1 = new CommandLine("sh") addArgument "proc-gid.sh" addArgument gid

		val executor = new DefaultExecutor
		val outputStream = new ByteArrayOutputStream
		val pumpsh = new PumpStreamHandler(outputStream, null)

		executor setStreamHandler pumpsh
		executor execute c1
		outputStream.toString
	}

	/**
	 * This class defines a <code>TypeFactoryImpl</code> for our <code>XmlRpcClient</code>.
	 *
	 * @param pController an instance of <code>XmlRpcController</code>
	 */
	class XmlRpcTypeFactory(pController: XmlRpcController) extends TypeFactoryImpl(pController) {
		/**
		 * Return a <code>TypeSerializer</code> object. For our purpose here, this method will return a <code>XmlRpcStringSerializer</code>
		 * object when the pObject is a String.
		 *
		 * @param pConfig a <code>org.apache.xmlrpc.common.XmlRpcStreamConfig</code> object
		 * @param pObject a Java Object
		 * @throws org.xml.sax.SAXException SAXException
		 * @return a <code>org.apache.xmlrpc.serializer.TypeSerializer</code> object
		 */
		@throws(classOf[SAXException])
		override def getSerializer(pConfig: XmlRpcStreamConfig, pObject: Object): TypeSerializer = {
			val response: TypeSerializer = pObject match {
				case s:String => new XmlRpcStringSerializer()
				case _ => super.getSerializer(pConfig, pObject)
			}
			response
		}
	}

	/**
	 * This class defines the behaviour of the <code>StringSerializer</code> used in the
	 * <code>XmlRpcTypeFactory</code>.
	 */
	class XmlRpcStringSerializer extends StringSerializer {
		@throws(classOf[SAXException])
		override def write(pHandler: ContentHandler, pObject: Object) {
			write(pHandler, StringSerializer.STRING_TAG, pObject.toString)
		}
	}

}

object OUtils extends Utils
